export class CpaasClient {
    private _bearerToken: string = '';

    constructor(bearerToken: string) {
        this._bearerToken = bearerToken;
    }

    get bearerToken(): string {return this._bearerToken;}

    set bearerToken(token: string) {this._bearerToken = token;}

    sendMessage(message: any) {
        return {
            bearerToken: this._bearerToken,
            message
        };
    }
}