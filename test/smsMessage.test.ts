import {SmsMessage, ContentType} from "../src";
import {expect} from "chai";

describe("SmsMessage", () => {
    it("Sets phone number and idempotencyKey when created", () => {
        const smsMessage = new SmsMessage('+14443332222');

        expect(smsMessage.phoneNumber).to.equal('+14443332222');
        expect(smsMessage.idempotencyKey).to.not.be.null;
    });

    it("sets and retrieves fields for message", () => {
        const smsMessage = new SmsMessage('+14443332222');

        smsMessage.id = "id123";
        smsMessage.contentType = ContentType.TEXT;
        smsMessage.content = "Hello World";
        smsMessage.phoneNumber = "+19998887777";

        expect(smsMessage.id).to.equal("id123");
        expect(smsMessage.contentType).to.equal(ContentType.TEXT);
        expect(smsMessage.content).to.equal("Hello World");
        expect(smsMessage.substitutions).to.be.empty;
        expect(smsMessage.phoneNumber).to.equal("+19998887777");
        expect(smsMessage.idempotencyKey).to.not.be.null;
    });

    it("sets substitutions properly", () => {
        const smsMessage = new SmsMessage('+14443332222');

        smsMessage.addSubstitution("name", "tester");
        smsMessage.addSubstitution("department", "testing");

        const correctSub = [
            {name: "tester"},
            {department: "testing"}
        ];

        expect(smsMessage.substitutions).to.be.deep.equal(correctSub);
    })
});