import {uuidv4} from "../helpers/identifiers";

export class SmsMessage {
    private _id: string = "";
    private _contentType: string = "";
    private _content: string = "";
    private _substitutions: Array<object> = [];
    private _phoneNumber: string = "";
    private _idempotencyKey: string = "";

    constructor(phoneNumber: string) {
        this._phoneNumber = phoneNumber;
        this._idempotencyKey = uuidv4();
    }

    get id():string {return this._id;}
    get contentType(): string {return this._contentType;}
    get content(): string {return this._content};
    get substitutions(): Array<object> {return this._substitutions;}
    get phoneNumber(): string {return this._phoneNumber;}
    get idempotencyKey(): string {return this._idempotencyKey;}

    set id(newId: string) {this._id = newId;}
    set contentType(newContentType: string) {this._contentType = newContentType;}
    set content(newContent: string) {this._content = newContent;}
    set phoneNumber(phoneNumber: string) {this._phoneNumber = phoneNumber;}

    addSubstitution(name: string, value: string) {
        this._substitutions.push({[name]: value});
    }
}