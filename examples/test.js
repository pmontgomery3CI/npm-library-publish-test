const {CpaasClient, SmsMessage, ContentType} = require('../dist/index');

//const {CpaasClient, SmsMessage, ContentType} = require('npm-library-publish-test')

const cpaasClient = new CpaasClient('bearer1234');

const message = new SmsMessage('+14443332222');
message.id = "12234";
message.contentType = ContentType.TEXT;
message.content = "Hello $(name), today is $(currDay)";
message.addSubstitution("name", "Phil");
message.addSubstitution("currDay", "Thursday");

const response = cpaasClient.sendMessage(message);

console.log(response);